FROM microsoft/dotnet:2.2-sdk AS build

# copy and build everything else
COPY ./src ./app
WORKDIR /app
RUN dotnet publish -c Release -o out

FROM microsoft/dotnet:2.2-runtime AS runtime
COPY --from=build /app/out /app
ENTRYPOINT [ "dotnet", "app/hellogitlab.dll" ]