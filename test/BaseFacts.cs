using System;
using Xunit;
using hellogitlab;

namespace hellogitlab.Test
{
    public class BaseFacts
    {
        [Fact]
        public void JustSuccess()
        {
            Assert.True(true);
        }

        [Fact]
        public void Clamp_Case()
        {
            var v = MyMath.Clamp(1.4f, 0.0f, 1.0f);
            Assert.Equal(1.0f, v);
        }
    }
}
