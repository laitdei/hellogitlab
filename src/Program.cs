﻿using System;

namespace hellogitlab
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello GitLab!");
        }
    }

    public static class MyMath
    {
        public static float Clamp(float value, float min, float max) 
        {
            if (value < min) return min;
            if (value > max) return max;
            return value;
        }
    }
}
